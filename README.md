## Использовано

- Redis 6.2
- Postgres 14.1
- Laravel 10.10
- PHP 8.2.8
- supervisord

## Запуск

1. Запускам докер контейнер

```bash
 docker-compose up -d --build
```

2. После запуска контейнера заходим в него и запускам консольную команду для парсинга курса за последние 180 дней от дня запуска. Курс парсится через задачи очереди 

```bash
 docker exec -it {containerId} /bin/sh

 php artisan app:currency-rates-init
```

3. (Опционально) Включен крон на ежедневное обновление курса, можно так же вызвать командой

```bash
 php artisan app:currency-rate-daily-update
```

## Документация

Документация api расположена по [ссылке](http://localhost:8082/docs/)

## Код
 * Консольные команды cbr-currency-app\app\Console\Commands 
 * Dto классы cbr-currency-app\app\Dto
 * Контроллер апи cbr-currency-app\app\Http\Controllers\Api\CurrencyController.php
 * Сервисы расчета и парсинга cbr-currency-app\app\Services
 * Тесты cbr-currency-app\tests\Unit
 
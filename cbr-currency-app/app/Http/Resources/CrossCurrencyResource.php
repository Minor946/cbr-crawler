<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CrossCurrencyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'date' => $this->getDate()->format('Y-m-d'),
            'crossRateName' => $this->getCrossRateName(),
            'currencyRate' => $this->getCurrencyRate(),
            'diffPrevDayCurrencyRate' => $this->getDiffPrevDayCurrencyRate(),
        ];
    }
}

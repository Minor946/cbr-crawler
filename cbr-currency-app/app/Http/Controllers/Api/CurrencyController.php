<?php

namespace App\Http\Controllers\Api;

use App\Enums\CurrencyEnum;
use App\Exceptions\CurrencyNotFoundException;
use App\Http\Resources\CrossCurrencyResource;
use App\Models\Currency;
use App\Services\CurrencyRateService;
use DateTime;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Response;
use Knuckles\Scribe\Attributes\QueryParam;
use Knuckles\Scribe\Attributes\Response as AttributesResponse;
use Knuckles\Scribe\Attributes\ResponseFromFile;

class CurrencyController extends BaseController
{
    #[QueryParam("currency", "Код валюты ISO 4217 .", required: true, example: "USD")]
    #[QueryParam("toCurrency", "Код базовой валюты  ISO 4217. Default: RUB", required: false, example: "SEK")]
    #[QueryParam("date", "Курс на указанную дату. Default today. Format: Y-m-d", required: false, example: "2023-07-18")]
    #[AttributesResponse(status: 422, description: "Currency not set", content: '{"message": "Currency not set"}')]
    #[AttributesResponse(status: 422, description: "Unsupported currency code", content: '{"message": "Unsupported currency code"}')]
    #[AttributesResponse(status: 400, description: "Incorrect date time format", content: '{"message": "Incorrect date time format"}')]
    #[AttributesResponse(status: 404, description: "Currency by this date not found", content: '{"message": "Currency by this date not found"}')]
    #[ResponseFromFile(file: "responses/example.json")]
    public function currencies(Request $request): JsonResource|JsonResponse
    {
        $date = $request->get('date', date('Y-m-d'));
        $currency = (string) $request->get('currency', null);
        $toCurrency = (string) $request->get('toCurrency', CurrencyEnum::RUB->name);
        if (!$currency) {
            return Response::json([
                'message' => 'Currency not set'
            ], 422);
        }
        /** @var Currency */
        $currencyModel = Currency::whereCharCode(strtoupper($currency))->first();
        if (!$currencyModel) {
            return Response::json([
                'message' => 'Unsupported currency code'
            ], 422);
        }
        $dateTime = DateTime::createFromFormat('Y-m-d', $date);
        if (!$dateTime) {
            return Response::json([
                'message' => 'Incorrect date time format'
            ], 400);
        }
        try {
            $result = (new CurrencyRateService($dateTime, $currency, $toCurrency))->getResult();
        } catch (CurrencyNotFoundException $e) {
            return Response::json([
                'message' => 'Currency by this date not found'
            ], 422);
        }
        return new CrossCurrencyResource($result);
    }
}

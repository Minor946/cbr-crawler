<?php

namespace App\Dto;

use DateTime;

final class CrossCurrencyRateDto
{
    public function __construct(
        private DateTime $date,
        private string $crossRateName,
        private float $currencyRate,
        private ?float $diffPrevDayCurrencyRate
    ) {
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function getCrossRateName(): string
    {
        return $this->crossRateName;
    }

    public function getCurrencyRate(): float
    {
        return $this->currencyRate;
    }

    public function getDiffPrevDayCurrencyRate(): ?float
    {
        return $this->diffPrevDayCurrencyRate;
    }
}

<?php

namespace App\Helpers;

class CurrencyRateHelper
{
    public static function getRate(float $currentCurrencyRate, ?float $toCurrencyRate = null): float
    {
        if (!$toCurrencyRate) {
            return $currentCurrencyRate;
        }
        return round((1 / $toCurrencyRate) / (1 / $currentCurrencyRate), 4);
    }
}

<?php

namespace App\Console\Commands;

use App\Jobs\ParseCurrencyRateDaily;
use DateTime;
use Illuminate\Console\Command;

class CurrencyRatesInit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:currency-rates-init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command init jobs for parse cbr currency for last six months';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        for($i = 0; $i < 180; $i++) {
            ParseCurrencyRateDaily::dispatch((new DateTime())->modify("-{$i} day"));
        }
    }
}

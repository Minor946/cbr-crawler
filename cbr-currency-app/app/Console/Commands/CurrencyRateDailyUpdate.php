<?php

namespace App\Console\Commands;

use App\Jobs\ParseCurrencyRateDaily;
use DateTime;
use Illuminate\Console\Command;

class CurrencyRateDailyUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:currency-rate-daily-update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Daily currency rate update';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        ParseCurrencyRateDaily::dispatch((new DateTime()));
    }
}

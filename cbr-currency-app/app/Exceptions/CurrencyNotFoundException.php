<?php

namespace App\Exceptions;

use Exception;

class CurrencyNotFoundException extends Exception
{
    function __construct($message = "Currency not found", $code = 404, $previous = null)
    { /* function body is hidden */
    }
}

<?php

namespace App\Interfaces;

interface CrossCurrencyRateInterface
{
    public function calculate(): ?float;
}

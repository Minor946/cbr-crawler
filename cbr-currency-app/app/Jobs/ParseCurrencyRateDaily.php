<?php

namespace App\Jobs;

use App\Services\ImportCurrencyRateService;
use DateTime;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ParseCurrencyRateDaily implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(private DateTime $dateTime)
    {
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        (new ImportCurrencyRateService($this->dateTime))->run();
    }
}

<?php

namespace App\Services;

use App\Interfaces\CrossCurrencyRateInterface;

class CrossCurrencyRateService
{
    public function handle(CrossCurrencyRateInterface $operation)
    {
        return $operation->calculate();
    }
}

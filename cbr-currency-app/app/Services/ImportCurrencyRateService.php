<?php

namespace App\Services;

use App\Dto\CurrencyRateDto;
use App\Models\Currency;
use App\Models\CurrencyRate;
use DateTime;

class ImportCurrencyRateService
{
    public function __construct(private DateTime $dateTime)
    {
    }

    public function run()
    {
        $currenciesXmlData = $this->getXmlData();
        if (!$currenciesXmlData) {
            return false;
        }

        $currenciesNumber = [
            'date' => $currenciesXmlData['date'],
            'processed' => 0,
            'imported' => 0,
            'failed' => 0,
            'errors' => []
        ];

        foreach ($currenciesXmlData['currencies'] as $currencyXmlElement) {
            /** @var $currencyXmlElement SimpleXMLElement */
            $currenciesNumber['processed']++;
            $currencyRate = $this->importRate(
                $currencyXmlElement,
                DateTime::createFromFormat('d.m.Y', $currenciesXmlData['date'])
            );
            if (is_string($currencyRate)) {
                $currenciesNumber['failed']++;
                $currenciesNumber['errors'][] = $currencyRate;
                continue;
            }

            $currenciesNumber['imported']++;
        }
        return $currenciesNumber;
    }

    /**
     * @inheritdoc
     */
    public function getXmlData()
    {
        libxml_use_internal_errors(true);
        $params = http_build_query(['date_req' => $this->dateTime->format('d/m/Y')]);
        $url = config('cbr.url') . "?{$params}";
        $currenciesXmlData = simplexml_load_file($url);
        if ($currenciesXmlData === false) {
            return false;
        }

        if (
            !$currenciesXmlData->count() ||
            !$currenciesXmlData->children()->count()
        ) {
            return false;
        }

        if (
            $currenciesXmlData->getName() != 'ValCurs' ||
            $currenciesXmlData->children()->getName() != 'Valute'
        ) {
            return false;
        }

        return [
            'date' => $currenciesXmlData->attributes()->Date->__toString(),
            'currencies' => $currenciesXmlData->children(),
        ];
    }

    public function importRate(
        \SimpleXMLElement $currencyXmlElement,
        DateTime $dateTime
    ) {
        $dto = new CurrencyRateDto($dateTime, $currencyXmlElement);

        $currency = Currency::where('internal_id', $dto->getValueId())
            ->orWhere('num_code', $dto->getNumCode())
            ->orWhere('char_code', $dto->getCharCode())
            ->first();
        if (!$currency) {
            $currency = new Currency;
            $currency->internal_id = $dto->getValueId();
            $currency->num_code = $dto->getNumCode();
            $currency->char_code = $dto->getCharCode();
            $currency->name = $dto->getName();
            if (!$currency->save()) {
                return 'Cannot save Currency into DB!';
            }
        }

        $currencyRate = CurrencyRate::where('currency_id', $currency->id)
            ->where('currency_date', $dateTime->format('Y-m-d'))
            ->first();
        if (!$currencyRate) {
            $currencyRate = new CurrencyRate;
            $currencyRate->currency_id = $currency->id;
            $currencyRate->currency_date = $dateTime->format('Y-m-d');
        }
        $currencyRate->nominal = $dto->getNominal();
        $currencyRate->value = $dto->getValue();
        if (!$currencyRate->save()) {
            return 'Cannot save Currency Rate into DB!';
        }
        return $currencyRate;
    }
}

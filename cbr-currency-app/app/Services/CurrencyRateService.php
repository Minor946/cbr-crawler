<?php

namespace App\Services;

use App\Dto\CrossCurrencyRateDto;
use App\Enums\CurrencyEnum;
use App\Exceptions\CurrencyNotFoundException;
use App\Helpers\CurrencyRateHelper;
use App\Models\CurrencyRate;
use App\Operations\CrossRateByOtherCurrency;
use App\Operations\CrossRateByRub;
use DateTime;

class CurrencyRateService
{
    public function __construct(
        private DateTime $dateTime,
        private string $currentCurrency,
        private string $toCurrency
    ) {
    }

    public function getResult(): ?CrossCurrencyRateDto
    {
        $todayCurrency = $this->getCurrencyRate($this->dateTime, $this->currentCurrency);
        if (!$todayCurrency) {
            throw new CurrencyNotFoundException();
        }
        $todayToCurrency = $this->getCurrencyRate($this->dateTime, $this->toCurrency);
        return new CrossCurrencyRateDto(
            date: $this->dateTime,
            crossRateName: $this->getCurrencyName(),
            currencyRate: CurrencyRateHelper::getRate($todayCurrency->value, $todayToCurrency->value ?? null),
            diffPrevDayCurrencyRate: $this->getCurrencyDiff($todayCurrency, $todayToCurrency)
        );
    }

    private function getCurrencyDiff(CurrencyRate $todayCurrency, ?CurrencyRate $todayToCurrency = null): ?float
    {
        $prevDateTime = (new DateTime())->setTimestamp($this->dateTime->getTimestamp());
        $prevDateTime->modify('-1 day');
        $prevCurrency = $this->getCurrencyRate($prevDateTime, $this->currentCurrency);
        if (!$prevCurrency) {
            return null;
        }
        if ($this->toCurrency === CurrencyEnum::RUB->name) {
            $operation = new CrossRateByRub(
                $todayCurrency->value,
                $prevCurrency->value
            );
        } else {
            $prevToCurrency = $this->getCurrencyRate($prevDateTime, $this->toCurrency);
            $operation = new CrossRateByOtherCurrency(
                $todayCurrency->value,
                $prevCurrency->value,
                $todayToCurrency->value,
                $prevToCurrency->value
            );
        }
        return (new CrossCurrencyRateService())->handle($operation);
    }

    private function getCurrencyName(): string
    {
        return sprintf('%s/%s', $this->currentCurrency, $this->toCurrency);
    }

    private function getCurrencyRate(
        DateTime $dateTime,
        string $currencyCharCode
    ): ?CurrencyRate {
        return CurrencyRate::query()
            ->with(['currency'])
            ->where('currency_date',  CurrencyRate::where('currency_date', '<=', $dateTime->format('Y-m-d'))
                ->max('currency_date'))
            ->whereRelation('currency', 'char_code', '=', $currencyCharCode)
            ->orderBy('currency_date', 'desc')
            ->first();
        // return Cache::remember(
        //     "currenciesRates-{$dateTime->format('Y-m-d')}-{$currencyCharCode}",
        //     3600 * 24,
        //     function () use (
        //         $dateTime,
        //         $currencyCharCode
        //     ) {

        //     }
        // );
    }
}

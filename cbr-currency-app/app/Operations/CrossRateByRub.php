<?php

namespace App\Operations;

class CrossRateByRub implements \App\Interfaces\CrossCurrencyRateInterface
{
    public function __construct(
        private float $currentCurrencyRate,
        private float $prevDateCurrencyRate,
    ) {
    }

    public function calculate(): ?float
    {
        return round(($this->currentCurrencyRate - $this->prevDateCurrencyRate), 4);
    }
}

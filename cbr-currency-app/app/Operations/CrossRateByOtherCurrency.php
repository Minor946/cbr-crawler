<?php

namespace App\Operations;

use App\Helpers\CurrencyRateHelper;

class CrossRateByOtherCurrency implements \App\Interfaces\CrossCurrencyRateInterface
{
    public function __construct(
        private float $currentCurrencyRate,
        private float $prevDateCurrencyRate,
        private float $toCurrencyRate,
        private float $prevDateToCurrencyRate
    ) {
    }

    public function calculate(): ?float
    {
        return round(
            (CurrencyRateHelper::getRate($this->currentCurrencyRate, $this->toCurrencyRate)
                -
                CurrencyRateHelper::getRate($this->prevDateCurrencyRate, $this->prevDateToCurrencyRate)
            ),
            4
        );
    }
}

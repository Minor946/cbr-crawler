<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->id();
            $table->string('internal_id', 8)->unique();;
            $table->string('num_code', 3)->unique()->comment('Currency code');
            $table->string('char_code', 3)->unique()->comment('Currency iso8601');
            $table->string('name')->unique()->comment('Currency name');

            $table->index(['internal_id', 'num_code', 'char_code']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('currencies');
    }
};

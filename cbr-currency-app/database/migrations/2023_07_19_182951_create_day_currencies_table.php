<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('currencies_rates', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('currency_id');
            $table->date('currency_date');
            $table->unsignedSmallInteger('nominal')->default(1)->comment('Currency nominal  value');
            $table->unsignedFloat('value', 8, 4)->comment('Currency value by nominal');

            $table->index(['currency_id', 'currency_date']);
            $table->index(['currency_date']);

            $table->timestamps();
            $table->unique(['currency_id', 'currency_date']);
            $table->foreign('currency_id')
                ->references('id')
                ->on('currencies')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('currencies_rates');
    }
};

<?php

namespace Tests\Unit;

use App\Operations\CrossRateByOtherCurrency;
use App\Operations\CrossRateByRub;
use App\Services\CrossCurrencyRateService;
use PHPUnit\Framework\TestCase;

class CrossCurrencyRateTest extends TestCase
{
    /**
     * A basic unit test example.
     */
    public function testRubCurrencyRate(): void
    {
        //Курс за 2023-07-21
        $usdCurrencyRate = 90.8545;
        $prevDateUsdCurrencyRate = 91.2046;
        $operation = new CrossRateByRub(
            $usdCurrencyRate,
            $prevDateUsdCurrencyRate
        );
        $value = (new CrossCurrencyRateService())->handle($operation);
        $this->assertEquals($value, -0.3501);
    }

    public function testOtherCurrencyRate(): void
    {
        //Курс за 2023-07-21
        $usdCurrencyRate = 90.8545;
        $prevDateUsdCurrencyRate = 91.2046;

        $eurCurrencyRate = 101.833;
        $prevDateEurCurrencyRate = 102.4441;
        $operation = new CrossRateByOtherCurrency(
            $usdCurrencyRate,
            $prevDateUsdCurrencyRate,
            $eurCurrencyRate,
            $prevDateEurCurrencyRate
        );
        $value = (new CrossCurrencyRateService())->handle($operation);
        $this->assertEquals($value, 0.0019);
    }
}

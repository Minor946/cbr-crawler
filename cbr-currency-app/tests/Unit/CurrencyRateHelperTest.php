<?php

namespace Tests\Unit;

use App\Helpers\CurrencyRateHelper;
use PHPUnit\Framework\TestCase;

class CurrencyRateHelperTest extends TestCase
{
    /**
     * A basic unit test example.
     */
    public function testCurrencyRateHelper(): void
    {
        //Курс за 2023-07-21
        $usdCurrencyRate = 90.8545;
        $this->assertEquals(CurrencyRateHelper::getRate($usdCurrencyRate), $usdCurrencyRate);
        $eurCurrency = 101.833;
        $this->assertEquals(CurrencyRateHelper::getRate($usdCurrencyRate, $eurCurrency), 0.8922);
    }
}

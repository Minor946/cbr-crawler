-- public.currencies definition

-- Drop table

-- DROP TABLE public.currencies;

CREATE TABLE public.currencies (
	id bigserial NOT NULL,
	internal_id varchar(8) NOT NULL,
	num_code varchar(3) NOT NULL, -- Currency code
	char_code varchar(3) NOT NULL, -- Currency iso8601
	"name" varchar(255) NOT NULL, -- Currency name
	created_at timestamp(0) NULL,
	updated_at timestamp(0) NULL,
	CONSTRAINT currencies_char_code_unique UNIQUE (char_code),
	CONSTRAINT currencies_internal_id_unique UNIQUE (internal_id),
	CONSTRAINT currencies_name_unique UNIQUE (name),
	CONSTRAINT currencies_num_code_unique UNIQUE (num_code),
	CONSTRAINT currencies_pkey PRIMARY KEY (id)
);
CREATE INDEX currencies_internal_id_num_code_char_code_index ON public.currencies USING btree (internal_id, num_code, char_code);

-- Column comments

COMMENT ON COLUMN public.currencies.num_code IS 'Currency code';
COMMENT ON COLUMN public.currencies.char_code IS 'Currency iso8601';
COMMENT ON COLUMN public.currencies."name" IS 'Currency name';


-- public.failed_jobs definition

-- Drop table

-- DROP TABLE public.failed_jobs;

CREATE TABLE public.failed_jobs (
	id bigserial NOT NULL,
	uuid varchar(255) NOT NULL,
	"connection" text NOT NULL,
	queue text NOT NULL,
	payload text NOT NULL,
	"exception" text NOT NULL,
	failed_at timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
	CONSTRAINT failed_jobs_pkey PRIMARY KEY (id),
	CONSTRAINT failed_jobs_uuid_unique UNIQUE (uuid)
);


-- public.migrations definition

-- Drop table

-- DROP TABLE public.migrations;

CREATE TABLE public.migrations (
	id serial4 NOT NULL,
	migration varchar(255) NOT NULL,
	batch int4 NOT NULL,
	CONSTRAINT migrations_pkey PRIMARY KEY (id)
);


-- public.password_reset_tokens definition

-- Drop table

-- DROP TABLE public.password_reset_tokens;

CREATE TABLE public.password_reset_tokens (
	email varchar(255) NOT NULL,
	"token" varchar(255) NOT NULL,
	created_at timestamp(0) NULL,
	CONSTRAINT password_reset_tokens_pkey PRIMARY KEY (email)
);


-- public.personal_access_tokens definition

-- Drop table

-- DROP TABLE public.personal_access_tokens;

CREATE TABLE public.personal_access_tokens (
	id bigserial NOT NULL,
	tokenable_type varchar(255) NOT NULL,
	tokenable_id int8 NOT NULL,
	"name" varchar(255) NOT NULL,
	"token" varchar(64) NOT NULL,
	abilities text NULL,
	last_used_at timestamp(0) NULL,
	expires_at timestamp(0) NULL,
	created_at timestamp(0) NULL,
	updated_at timestamp(0) NULL,
	CONSTRAINT personal_access_tokens_pkey PRIMARY KEY (id),
	CONSTRAINT personal_access_tokens_token_unique UNIQUE (token)
);
CREATE INDEX personal_access_tokens_tokenable_type_tokenable_id_index ON public.personal_access_tokens USING btree (tokenable_type, tokenable_id);


-- public.users definition

-- Drop table

-- DROP TABLE public.users;

CREATE TABLE public.users (
	id bigserial NOT NULL,
	"name" varchar(255) NOT NULL,
	email varchar(255) NOT NULL,
	email_verified_at timestamp(0) NULL,
	"password" varchar(255) NOT NULL,
	remember_token varchar(100) NULL,
	created_at timestamp(0) NULL,
	updated_at timestamp(0) NULL,
	CONSTRAINT users_email_unique UNIQUE (email),
	CONSTRAINT users_pkey PRIMARY KEY (id)
);


-- public.currencies_rates definition

-- Drop table

-- DROP TABLE public.currencies_rates;

CREATE TABLE public.currencies_rates (
	id bigserial NOT NULL,
	currency_id int4 NOT NULL,
	currency_date date NOT NULL,
	nominal int2 NOT NULL DEFAULT '1'::smallint, -- Currency nominal  value
	value float8 NOT NULL, -- Currency value by nominal
	created_at timestamp(0) NULL,
	updated_at timestamp(0) NULL,
	CONSTRAINT currencies_rates_currency_id_currency_date_unique UNIQUE (currency_id, currency_date),
	CONSTRAINT currencies_rates_pkey PRIMARY KEY (id),
	CONSTRAINT currencies_rates_currency_id_foreign FOREIGN KEY (currency_id) REFERENCES public.currencies(id) ON DELETE CASCADE
);
CREATE INDEX currencies_rates_currency_date_index ON public.currencies_rates USING btree (currency_date);
CREATE INDEX currencies_rates_currency_id_currency_date_index ON public.currencies_rates USING btree (currency_id, currency_date);

-- Column comments

COMMENT ON COLUMN public.currencies_rates.nominal IS 'Currency nominal  value';
COMMENT ON COLUMN public.currencies_rates.value IS 'Currency value by nominal';

INSERT INTO public.migrations (migration,batch) VALUES
	 ('2014_10_12_000000_create_users_table',1),
	 ('2014_10_12_100000_create_password_reset_tokens_table',1),
	 ('2019_08_19_000000_create_failed_jobs_table',1),
	 ('2019_12_14_000001_create_personal_access_tokens_table',1),
	 ('2023_07_19_153415_create_currencies_table',1),
	 ('2023_07_19_182951_create_day_currencies_table',1);
